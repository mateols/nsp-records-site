import passport from 'passport';
import initPassport from './passport';
import { getConnectionPool, isLoggedIn } from '../util';
import session from 'express-session';

import {
	logout,
	authenticateLocally,
	authenticateWithGoogle,
	googleCallbackHook,
	getLoginView,
	getUser,
} from './AuthenticationController';

export default app => {
	initPassport(passport, getConnectionPool());
	app.use(session({secret:"everystudent2017"}));
	app.use(passport.initialize());
	app.use(passport.session());

	app.route("/api/user")
		.get(isLoggedIn, getUser);

	app.route("/login")
		.get(getLoginView)

	app.route("/logingoogle")
		.get(authenticateWithGoogle(passport))

	app.route('/googlecallback')
		.get(googleCallbackHook(passport));

	app.route("/logout")
		.get(logout)

	app.route('/loginlocal')
		.post(authenticateLocally(passport));
}
