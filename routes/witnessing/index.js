import { withDBConnection, isLoggedIn } from '../util';
import {
	getWitnessingDaysByChapterId,
	getWitnessingTrainingsByChapterId,
	getWitnessingDaysBySchoolId,
	getWitnessingDay,
	getWitnessingDays
} from './WitnessingController';

export default app => {
	app.route('/api/witnessing_day/:ids')
		.get(isLoggedIn, withDBConnection(getWitnessingDaysByChapterId));

	app.route('/api/witnessing_training/:ids')
		.get(isLoggedIn, withDBConnection(getWitnessingTrainingsByChapterId));

	app.route('/api/witnessing_days/school/:id')
		.get(isLoggedIn, withDBConnection(getWitnessingDaysBySchoolId));

	app.route('/api/witnessing_days/:id')
		.get(isLoggedIn, withDBConnection(getWitnessingDay));

	app.route('/api/witnessing_days')
		.get(isLoggedIn, withDBConnection(getWitnessingDays));
}
