import {
	getPermissions,
	returnSQLResponse
} from '../util';
import moment from 'moment';

export const getWitnessingTrainingsByChapterId = (request, response, connection) => {
	let {ids} = request.params;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT witnessing_training.*
		FROM
			witnessing_training
			JOIN
			school_registration
			ON
			witnessing_day.schoolreg_id = school_registration.registration_id
		WHERE 
			chapter_id IN(${ids})
			AND (
				school_registration.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)`,
		returnSQLResponse(connection, response)
	);
};

export const getWitnessingDaysBySchoolId = (request, response, connection) => {
	let {id} = request.params;
	const { startDate, endDate } = request.query;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT
			schools.name,
			schools.id,
			witnessing_day.location,
			witnessing_day.id,
			witnessing_day.date,
			witnessing_day.gospel_presentations,
			witnessing_day.indicated_decisions,
			chapters.name AS 'chapter_name',
			witnessing_day.chapter_id
		FROM
			witnessing_day
			JOIN
			school_registration
				ON
				witnessing_day.schoolreg_id = school_registration.registration_id
			JOIN
			schools
				ON
				school_registration.school_id = schools.id
			JOIN
			chapters
				ON
				chapters.id = witnessing_day.chapter_id
		WHERE
			schools.id = ${id}
			AND (
				witnessing_day.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)
			${startDate ? `AND date >= "${moment(startDate).format('YYYY-MM-DD')}"` : ''}
			${endDate ? `AND date <= "${moment(endDate).format('YYYY-MM-DD')}"` : ''};`,
		returnSQLResponse(connection, response)
	)
}

export const getWitnessingDaysByChapterId = (request, response, connection) => {
	let {ids} = request.params;
	const { startDate, endDate } = request.query;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess,
	} = getPermissions(request);

	connection.query(
		`SELECT
			witnessing_day.*,
			chapters.name as 'chapter_name',
			school_registration.school_id,
			schools.name
		FROM (
			(
				(
					school_registration
					INNER JOIN
					schools
						ON school_registration.school_id = schools.id
				)
				INNER JOIN
				chapters
					ON school_registration.chapter_id = chapters.id
			)
			INNER JOIN
			witnessing_day
				ON school_registration.registration_id = witnessing_day.schoolreg_id
		)
		WHERE
			witnessing_day.chapter_id IN (${ids})
			AND (
				witnessing_day.chapter_id IN ${chaptersSQL}
     			OR
      			school_registration.school_id IN ${schoolsSQL}
      			OR
      			${hasFullAccess}
			)
			${startDate ? `AND date >= "${moment(startDate).format('YYYY-MM-DD')}"` : ''}
			${endDate ? `AND date <= "${moment(endDate).format('YYYY-MM-DD')}"` : ''}`,
		returnSQLResponse(connection, response)
	);
};

export const getWitnessingDay = (request,response,connection) => {
	let {id} = request.params;

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess
	} = getPermissions(request);

	connection.query(
		`SELECT
			witnessing_day.*, 
			schools.name AS 'school_name',
			chapters.name AS 'chapter_name',
			schools.id AS 'school_id',
			chapters.id AS 'chapter_id'
		FROM
			witnessing_day
			JOIN
			school_registration
				ON witnessing_day.schoolreg_id = school_registration.registration_id 
			JOIN 
			schools
				ON school_registration.school_id = schools.id
			JOIN
			chapters
				ON witnessing_day.chapter_id = chapters.id 
		WHERE
			witnessing_day.id = ${id}
			AND (
				witnessing_day.chapter_id IN ${chaptersSQL}
				OR
				school_registration.school_id IN ${schoolsSQL}
				OR
				${hasFullAccess}
			)`,
		returnSQLResponse(connection, response)
	)
}

export const getWitnessingDays = (request,response,connection) => {

	const {
		schoolsSQL,
		chaptersSQL,
		hasFullAccess,
	} = getPermissions(request);

	connection.query(
		`SELECT
			witnessing_day.*,
			chapters.name as 'chapter_name',
			school_registration.school_id,
			schools.name
		FROM
			witnessing_day
			JOIN
			school_registration
				ON witnessing_day.schoolreg_id = school_registration.registration_id
			JOIN
			schools
				ON school_registration.school_id = schools.id
			JOIN
			chapters
				ON witnessing_day.chapter_id = chapters.id
		WHERE
			witnessing_day.chapter_id IN ${chaptersSQL}
			OR
			school_registration.school_id IN ${schoolsSQL}
			OR
			${hasFullAccess}
		ORDER BY witnessing_day.date DESC`,
			returnSQLResponse(connection, response)
	)
}
