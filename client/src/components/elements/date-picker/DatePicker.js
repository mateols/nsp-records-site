import React, {Component} from 'react';

/**
 * @description A simple component for picking dates
 */
class DatePicker extends Component {
	render() {
		return (
			<div>
				<div className="mdl-textfield mdl-js-textfield">
					<p>{this.props.label}</p>
					<input className="mdl-textfield__input" defaultValue={this.props.value} onChange={this.props.onChange} id={this.props.inputId} type="date"  />
				</div>
			</div>
		)
	}
}

export default DatePicker;
